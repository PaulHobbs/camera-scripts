from typing import Optional, Callable
from watcher import watch
from pathlib import Path
import shutil
import subprocess
import re
import os
import threading

ORIGINALS_DIR = Path('/media/hdd/shared/Pictures/Originals')
CAM_DIR = Path('.')
_IGNORED_SUFFIXES = frozenset((
    '.mp4_exiftool_tmp',
    '.mp4_original',
))
_IGNORED_PREFIXES = frozenset(('.'))


def organize_originals():
    organize(ORIGINALS_DIR)

def organize_cam():
    organize(CAM_DIR, _set_exif_from_original)


def _set_exif_from_original(path: Path):
    if path.suffix != 'mp4': return

    orig = _find_original(path)
    if orig is not None:
        try:
            subprocess.check_call(['exiftool', '-tagsfromfile', str(orig), '-all:all', str(path)])
            path.with_suffix('.mp4_original').unlink()
        except Exception as e:
            print(f'Got exception calling exiftool: {e}')
    else:
        print(f'could not find original for {path}')


def _find_original(path: Path) -> Optional[Path]:
    def look(path: Path):
        unmoved = ORIGINALS_DIR / path
        if unmoved.exists():
            return unmoved
        print(f'unmoved does not exist: {unmoved}')
        date = date_subdir(path.name)
        if not date: return None
        moved = ORIGINALS_DIR / date / path
        if moved.exists():
            return moved
        print(f'moved does not exist: {unmoved}')

    path = path.with_suffix(path.suffix.upper())  # originals have .MP4 not .mp4
    p = look(path)
    if p: return p
    return look(_strip_movie_suffix(path))


_MOVIE_STEM_SUFFIX = '-s'
def _strip_movie_suffix(path: Path) -> Path:
    stripped = path.stem
    if stripped.endswith(_MOVIE_STEM_SUFFIX):
        stripped = stripped[:-len(_MOVIE_STEM_SUFFIX)]
    return path.parent / (stripped + path.suffix)



def organize(destination: Path, callback: Callable[[Path], None]=lambda _: None):
    print(f'watching {destination}...')
    changes = watch(directory=str(destination), quiet_interval=60)
    for events in changes:
        for f in filter(is_important_file, events):
            print(f'Got file: {f}')
            callback(f)
            _organize_file(f, destination)


def is_important_file(f: Path):
    return (f.suffix not in _IGNORED_SUFFIXES
            and not any(f.stem.startswith(pre) for pre in _IGNORED_PREFIXES))


def _organize_file(path: Path, destination: Path) -> Optional[Path]:
    subdir = date_subdir(path.name)
    if subdir is None:
        _exiftool_organize(path)
        return None
    d = destination / subdir
    if not d.exists():
        os.makedirs(d)
    shutil.move(str(path), d)
    return d


def _exiftool_organize(path: Path):
    try:
        subprocess.check_call([
            'exiftool', '-Directory<DateTimeOriginal', '-d', '%Y/%m/%d',
            str(path)])
    except Exception as e:
        print(f'got exception moving file with exiftool: {e}')


_DATE_RE = re.compile(r'(\d\d\d\d)(\d\d)(\d\d)')


def date_subdir(file_name: str) -> Optional[Path]:
    """Extracts the date from a photo path and returns the subdirectory where it belongs."""
    prefix_match = _DATE_RE.match(file_name)
    if not prefix_match: return None
    yyyy, mm, dd = prefix_match.groups()
    return Path('/'.join((yyyy, mm, dd)))


def argparser():
    import argparse
    parser = argparse.ArgumentParser(description='Organize media files.')
    parser.add_argument('files', type=str, nargs='*', help='files to organize')
    parser.add_argument('--original', default=False, type=bool, help='Whether to treat the file as an original. If false, copies metadata from Originals dir.')
    return parser

def main():
    parser = argparser()
    args = parser.parse_args()

    if args.files:
        print(f'organizing files {args.files}...')
        for f in args.files:
            if not args.original:
                _set_exif_from_original(Path(f))
            _organize_file(Path(f), CAM_DIR)
        return

    threading.Thread(target=organize_cam).start()
    organize_originals()

if __name__ == '__main__':
    main()
