import unittest
import watcher
import itertools

class WatcherTest(unittest.TestCase):
    
    def test_merge(self):
        x = [1,2,3]
        y = [4,5,6]
        xys = watcher.merge(x,y)
        expected = set(range(1,7))
        self.assertSetEqual(expected & set(itertools.islice(xys, 6)), expected)


if __name__ == '__main__':
        unittest.main(verbosity=2)
