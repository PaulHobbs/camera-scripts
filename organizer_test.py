import unittest
import pathlib
import organizer

class OrganizerTest(unittest.TestCase):
    
    def test_is_important_file(self):
        fs = ['foo.mp4', 'foo.jpg', 'foo.raw', 'foo.MP4', '.foo.mp4', 'foo.mp4_exiftool_tmp']
        expected = [True, True, True, True, False, False]
        for f, want in zip(fs, expected):
            got = organizer.is_important_file(pathlib.Path(f))
            self.assertEqual(got, want, f'want is_important_file({f}) == {want}, got={got}')

if __name__ == '__main__':
        unittest.main(verbosity=2)
