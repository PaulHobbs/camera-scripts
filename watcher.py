from typing import Iterable, List, TypeVar
import subprocess
import pathlib
import timeout_iterator
from queue import Queue
import threading


CMD = 'inotifywait -m -e CLOSE_WRITE --format %w%f'.split()


def execute(cmd: List[str]):
    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
    if popen.stdout is None: return
    for stdout_line in iter(popen.stdout.readline, ""):
        yield stdout_line[:-1]  # strip the last newline character
    popen.stdout.close()
    return_code = popen.wait()
    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)


def watch(directory: str, quiet_interval: int) -> Iterable[List[pathlib.Path]]:
    stream = execute(CMD + [directory])
    stream = timeout_iterator.TimeoutIterator(stream, timeout=quiet_interval, sentinel=None)
    events: List[pathlib.Path] = []
    for e in stream:
        if not e:  # timeout
            if events:
                yield events
            events = []
            continue

        # When initially copying files over, some ghost placeholder files are created.
        # We don't want to move the ghost files over, so we skip over any small files.
        p = pathlib.Path(e)
        if not size_greater_than(p, 10000):
            continue
        else:
            events.append(pathlib.Path(e))

    print('done?')


def size_greater_than(p: pathlib.Path, n_bytes: int) -> bool:
    stat = p.stat()
    return stat and stat.st_size < 10000


X = TypeVar('X')

def merge(left: Iterable[X], right: Iterable[X]) -> Iterable[X]:
    q = Queue()
    def consume(stream: Iterable[X]):
        for e in stream:
            q.put(e)
    threading.Thread(target=consume, args=(left,)).start()
    threading.Thread(target=consume, args=(right,)).start()
    while True:
        yield q.get()


if __name__ == '__main__':
    for e in watch('.', quiet_interval=15):
        print(e)
