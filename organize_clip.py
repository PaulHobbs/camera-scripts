from watcher_poll import watch, filter_finished
from typing import List
from pathlib import Path
from shutil import copy2
import constants
import threading
import sqlite3
import argparse
import organizer
import time
import queue
import subprocess


def transfer_file(mp4:Path, dest: str):
    start = time.time()
    print(f'transferring {mp4.name}...')
    p = subprocess.Popen(['scp', str(mp4), 'root@openmediavault.local:/media/hdd/shared/Pictures/' + dest],
        stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
    p.communicate()
    print(f'Transferring {mp4.name} took {int(time.time() - start)}s.')


def ingress():
    xmls = watch(constants.CHIP_DIR, '*.XML')

    # Have a separate thread for transferring to the NAS to optimize throughput.
    # Copying all the mp4s from the chip onto local disk immediately allows
    # removing the chip as quickly as possible, while allowing more time to copy
    # to NAS (which can be very slow).
    q = queue.Queue()
    def transfer():
        while True:
            mp4: Path = q.get()
            print(f'transfer queue length = {q.qsize()}')
            transfer_file(mp4, 'Originals')

    threading.Thread(target=transfer).start()

    for batch in xmls:
        batch = list(filter(organizer.is_important_file, batch))
        if not batch: continue
        batch.sort()
        print(f'Got XMLs: {batch}')
        for xml in batch:
            mp4 = xml.with_suffix('.MP4')
            mp4 = mp4.with_name(mp4.name.replace('M01.', '.')) 
            if not mp4.exists():
                print(f"WEIRD.... the video {mp4.name} does not exist for XML file {xml.name}")
                xml.unlink()
                continue

            copy2(mp4, constants.UNPROCESSED)
            mp4 = constants.UNPROCESSED / mp4.name
            q.put(mp4)
            xml.unlink()
            

def egress(conn: sqlite3.Connection):
    mp4s = map(filter_finished, watch(constants.RECOLORED, '*.mp4', interval=30))
    for batch in mp4s:
        untransferred = find_untransferred(conn, batch)
        if not untransferred:
            continue
        for mp4 in untransferred:
            print(f'copying {mp4.name} to {constants.MEDIA_CAM}')
            transfer_file(mp4, 'Cam')
        insert_transferred(conn, untransferred)


def find_untransferred(conn: sqlite3.Connection, batch: List[Path]) -> List[Path]:
    questions = ','.join(['?']*len(batch))
    query = f'SELECT file FROM transferred WHERE file IN ({questions})'
    cursor = conn.execute(query, [str(f.name) for f in batch])
    found = set(row[0] for row in cursor.fetchall())
    cursor.close()
    return [f for f in batch if str(f.name) not in found]


def insert_transferred(conn: sqlite3.Connection, batch: List[Path]):
    questions = ','.join(['(?)']*len(batch))
    q = f"INSERT INTO transferred ('file') VALUES {questions}"
    cursor = conn.execute(q, [str(f.name) for f in batch])
    conn.commit()
    cursor.close()


def main():
    threading.Thread(target=ingress).start()

    parser = argparse.ArgumentParser()
    parser.add_argument('db', type=str, help='db path')
    args = parser.parse_args()

    conn = sqlite3.connect(args.db)
    egress(conn)

if __name__ == '__main__': main()