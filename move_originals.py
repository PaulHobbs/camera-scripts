#!/bin/python3
"""
A temporary script for 1-time moving of originals from the Cam directory
to their appropriate place in the Originals directory.
"""

from typing import Iterable
import os
from pathlib import Path
import shutil

def originals(files):
    names = set(f.name for f in files)
    for f in files:
        if f.name.startswith('.'):
            continue
        if with_suffix(f) in names:
            yield f


def with_suffix(f: Path) -> str:
    return f.stem + '-s' + f.suffix.lower()


def find_originals(d: Path, extension: str) -> Iterable[Path]:
    dirs = d.glob('**')
    for subdir in dirs:
        files = list(subdir.glob('*.' + extension))
        files += list(subdir.glob('*.' + extension.lower()))
        yield from originals(files)


def main():
    for orig in find_originals(Path('.'), 'MP4'):
        print(str(orig))
        new = '../Originals/' + str(orig)
        print(str(new))
        os.makedirs(os.path.dirname(new), exist_ok=True)
        shutil.move(orig, new)


if __name__ == '__main__':
    main()
