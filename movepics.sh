#!/bin/bash

function handle () {
	# TODO handle dates from movies too
	dates=$(exiftool '-DateTimeOriginal' -d "%Y/%m/%d" *.ARW *.JPG | awk '/Original/ { print $4 }' | sort | uniq)
	mdates=$(find -maxdepth 1 -iname '*.MP4' -type f | sed 's:\./\([0-9]\{8\}\)_.*:\1:g' | sort | uniq)

	# Only run exiftool for videos when videos are present at the top-level.
	shopt -s nullglob
	set -- *.mp4 *.MP4
	files=$(ls *.mp4 *.MP4 *.ARW *.JPG *.mp4)
	if [ "$#" -gt 0 ]; then
		exiftool "-Directory<MediaCreateDate" -d "%Y/%m/%d" *.MP4 *.mp4	
	fi

	# Exiftool for images
	set -- *.ARW *.JPG
	if [ "$#" -gt 0 ]; then
		exiftool "-Directory<DateTimeOriginal" -d "%Y/%m/%d" *.ARW *.JPG
	fi

	echo "Syncing photostructure for $dates:"
	docker exec -it photostructure_photostructure_1 /ps/app/photostructure sync $(for d in $dates; do find $d -type f | sed -e 's:\(\S*\):/var/Pictures/Cam/\1:g'; done) --exitWhenDone

	echo "Refreshing nextcloud for $dates:"
	# for dir in $dates; do
	# 	snap run nextcloud.occ files:scan "--path=/phobbs/files/Photos/$dir"
	# done

	chown -R family .
}


# An initial sync when the script starts makes it easier to check that it works.
handle

while inotifywait -e CLOSE_WRITE --format %w%f .
do
	# TODO: fix behavior when duplicates exist.
	sleep 400  # Allow some time for interrupted downloads to complete.
	handle
done
