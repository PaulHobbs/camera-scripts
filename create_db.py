import sqlite3
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('db', type=str, help='db path')
    args = parser.parse_args()

    conn = sqlite3.connect(args.db)
    schema = open('schema.sql').read()
    conn.execute(schema).close()

if __name__ == '__main__': main()