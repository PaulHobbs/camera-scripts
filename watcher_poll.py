"""Directory polling utility function 'watch' returns files matching a pattern."""
from typing import List, TypeVar, Callable, Iterable
import functools
import time
from pathlib import Path


def watch(path: Path, pattern: str, interval: int = 10):
    return poll(functools.partial(watch_once, path, pattern), interval)


def watch_once(path, pattern: str) -> List[Path]:
    if not path.exists():
        return []

    return list(path.glob(pattern))


X = TypeVar('X')
def poll(f: Callable[[], X], interval: int) -> Iterable[X]:
    while True:
        start = time.time()
        yield f()
        sleep_time = max(0, interval - (time.time() - start))
        time.sleep(sleep_time)


def filter_finished(files: List[Path], interval: int = 5) -> List[Path]:
    stats = {f: f.stat() for f in files}
    time.sleep(interval)
    ret = []
    for f in files:
        stat = f.stat()
        if stat and stats[f]:
            if stat.st_mtime_ns == stats[f].st_mtime_ns:
                ret.append(f)
            else:
                print(f'Skipping file still in progress: {f.name}')

    return ret