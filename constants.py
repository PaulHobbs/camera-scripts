from pathlib import Path

# For MacOS
CHIP_DIR = Path('/Volumes/PictureChip/PRIVATE/M4ROOT/CLIP')
UNPROCESSED = Path('/Users/phobbs/Documents/Sony/originals/unprocessed')
PROCESSED = Path('/Users/phobbs/Documents/Sony/originals/unprocessed')
RECOLORED = Path('/Users/phobbs/Documents/Sony/recolored')
MEDIA_PICTURES = Path('/Volumes/Media/Pictures')
MEDIA_ORIGINALS = MEDIA_PICTURES / 'Originals'
MEDIA_CAM = MEDIA_PICTURES / 'Cam'
